# PACTE-AAP

Cette feuille de calcul est nécessaire pour l'instruction des dossiers financés par le PACTE du Plan de Relance de l'ETAT dans le cadre des AAP Plan de compétitivité des exploitations agricoles de la Région des Pays de la Loire 2021-2022.

Etape 1 : analyse de l'existant
Etape 2 : réécriture de sous programmes en langage  LibreOffice BASIC

## Liste des Sub :

Feuille 3

- CommandButton1_Click()
    - Call PCAE_feuille_calc_2021

Module 1

- Sub PCAE_feuille_calc_2021_calAide()
    - Call remettre_à_zéro
    - call PCAE_feuille_calc_2021_taux50
    - Call export_csv
- Sub remettre_à_zéro()
- Sub remettre_à_zéro_onglet_dépenses()
- Sub export_csv()
- Sub ChoixRepertoire()
- Sub essaival()

Module2:

- Sub PCAE_feuille_calc_2021_taux50()


## liste des feuilles et de boutons

### Feuil5 Nota

Note explicative

### Feuil4 demande

cellules A4:A16

- Département
- Nom / Raison sociale
- siret
- Numéro Osiris
- Société
- nombre d'associés
- JA donnant lieu à majoration
- parts sociales détenues par le(s) JA
- Mise aux normes nitrates
- filière
- projet
- note attribuée au projet
- description du projet


### Feuil3 dépenses

- Bouton 24
    - calculer le taux "50%"
    - VBAProject.Module2.PCAE_feuille_calc_2021_taux50 (document, Basic)
    
- Bouton 31
    - Effacement de toutes les donn es du tableau
    - (pas d'évènements rattachés)


### Feuil2 Synthèse

- Bouton 1
    - Calculer le montant de l'aide
    - VBAProject.Module1.PCAE_feuille_calc_2021_calAide (document, Basic)
    
- Bouton 2
    - remettre z ro
    - (pas d'évènements rattachés)


### Feuil7 dépenses osiris

## feuilles cachées
### Feuil9 note
3 lignes
### Feuil1 références
63 lignes
### Feuil6 types de projets
15 lignes
### Feuil8 grilles taux_plafonds
65 lignes

