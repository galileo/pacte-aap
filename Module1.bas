REM  *****  BASIC  *****

Option Explicit 

Sub SpReZeroFeuilleDepense()

	Dim objFeuille as Object
	Dim objCellules as Object

	intMsgType = 4+ 32 '4=oui/non 32=question
	intChoix = MsgBox("Etes vous sûr de vouloir effacer toutes les données du tableau de dépenses ?", intMsgType)
	
	Select Case inChoix
		Case 6 'réponse oui
		
		objFeuille = ThisComponent.Sheets.getByName("dépenses")
		ThisComponent.CurrentController.setActiveSheet(objFeuille)
			
		objCellules = objFeuille.getCellRangeByName("B3:M55")
		ThisComponent.CurrentController.selec(objCellules)
		Case 7 'réponse non
			'rien à faire
	End Select

End Sub

