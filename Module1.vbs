Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1


Sub PCAE_feuille_calc_2021_calAide()
Dim tabl()

'la macro permet de calculer l'aide et d'établir les informations de synthèse

'remise à zéro des résultats de l'onglet synthèse :
 Call remettre_à_zéro
'calcule le % BBEA
Call PCAE_feuille_calc_2021_taux50
'référencement du tableau de REFERENCES, onglet références ref

nomfich = "références"
Sheets(nomfich).Activate

ligzref = Cells(2000, 1).End(xlUp).Row

Set plageref = Range(Cells(ligzref, 1), Cells(ligzref, 1)).CurrentRegion

'plageref.Select
ad = plageref.Address
pos1 = InStr(1, ad, "$")
pos2 = InStr(pos1 + 1, ad, "$")
pos3 = InStr(pos2 + 1, ad, "$")
pos4 = Len(ad) - (pos3)
Set premcellref = Range(Left(ad, pos3 - 2))
Set derncellref = Range(Right(ad, pos4))
premcellref.Select
'derncellref.Select

ligaref = premcellref.Row
colaref = premcellref.Column
ligzref = derncellref.Row
colzref = derncellref.Column

Set plageref = Range(Cells(ligaref, 1), Cells(ligzref, 1))
For Each cell In plageref
    If cell = "filière" Then
    colfilièrref = cell.Column
    End If
    If cell = "Entreprise" Then
    colentref = cell.Column
    End If
    If cell = "Nouvelles sous-opérations" Then
    colsousopref = cell.Column
    End If
 Next



'référencement du tableau de DEMANDE, onglet DEMANDE
nomfich = "demande"
Sheets(nomfich).Activate


Set plage1 = Range(Cells(4, 1), Cells(15, 2))

'plage1.Select
ad = plage1.Address
pos1 = InStr(1, ad, "$")
pos2 = InStr(pos1 + 1, ad, "$")
pos3 = InStr(pos2 + 1, ad, "$")
pos4 = Len(ad) - (pos3)
Set premcell = Range(Left(ad, pos3 - 2))
Set derncell = Range(Right(ad, pos4))
premcell.Select
'derncell.Select

liga1 = premcell.Row
cola1 = premcell.Column
ligz1 = derncell.Row
colz1 = derncell.Column


'référencement  : identification des étiquettes"
            
                Set plage1 = Range(Cells(liga1, 1), Cells(ligz1, 1))
                For Each cell In plage1
                    If cell = "Département" Then
                    ligdép = cell.Row
                    End If
                    If cell = "Numéro Osiris" Then
                    lignumosi = cell.Row
                    End If
                    If cell = "Société" Then
                    ligsoc = cell.Row
                    End If
                    If cell = "nombre d'associés" Then
                    lignbass = cell.Row
                    End If
                    If cell = "JA donnant lieu à majoration" Then
                    ligja = cell.Row
                    End If
                    If cell = "parts sociales détenues par le(s) JA" Then
                    ligpja = cell.Row
                    End If
                    If cell = "projet" Then
                    lignatproj = cell.Row
                    End If
                    If cell = "filière" Then
                    ligfil = cell.Row
                    End If
                    If cell = "note attribuée au projet" Then
                    lignote = cell.Row
                    End If
                Next cell
             
'fin de réferencement
' collecte des informations du demandeur
'lignumosi
dnumosi = Cells(lignumosi, 2)
'société
dsociété = Cells(ligsoc, 2)
'nombre d'associés
dnbassos = Cells(lignbass, 2)
'ja
dja = Cells(ligja, 2)
'parts sociales ja
dpja = Cells(ligpja, 2)
'filière
dfil = Cells(ligfil, 2)
'projet
natproj = Cells(lignatproj, 2)
'département
département = Cells(ligdép, 2)
'note
note = Cells(lignote, 2)

'travail sur le tableau des TAUX ET PLAFONDS, onglet grilles TAUX ET PLAFONDS

nomfich = "grilles taux_plafonds"
Sheets(nomfich).Activate

ligz3 = Cells(Rows.Count, 1).End(xlUp).Row
Set plage3 = Range(Cells(ligz3, 1), Cells(ligz3, 1)).CurrentRegion
'plage3.Select
ad = plage3.Address
pos1 = InStr(1, ad, "$")
pos2 = InStr(pos1 + 1, ad, "$")
pos3 = InStr(pos2 + 1, ad, "$")
pos4 = Len(ad) - (pos3)
Set premcellG = Range(Left(ad, pos3 - 2))
Set derncellG = Range(Right(ad, pos4))
liga3 = premcellG.Row
cola3 = premcellG.Column
ligz3 = derncellG.Row
colz3 = derncellG.Column

Set plage3a = Range(Cells(liga3, 1), Cells(liga3, colz3))
For Each cell In plage3a
    If cell = "sous opération" Then
    colsousopP = cell.Column
    End If
    If cell = "filières" Then
    colfilierP = cell.Column
    End If
    If cell = "types projets" Then
    coltyproj = cell.Column
    End If
    If cell = "projets" Then
    colproj = cell.Column
    End If
    If cell = "Taux" Then
    coltauxP = cell.Column
    End If
    If cell = "Man" Then
    colman = cell.Column
    End If
    If cell = "Plafond" Then
    colplafP = cell.Column
    End If
    If cell = "types socle" Then
    coltypsoc = cell.Column
    End If
 Next



'travail sur les dépenses, onglet DEPENSES

nomfich = "dépenses"
Sheets(nomfich).Activate

'référencement  : identification des étiquettes", onglet DEPENSES dep


ligz = Cells(2000, 5).End(xlUp).Row

Set plagedep = Range(Cells(3, 1), Cells(ligz, 13))

'plagedep.Select
ad = plagedep.Address
pos1 = InStr(1, ad, "$")
pos2 = InStr(pos1 + 1, ad, "$")
pos3 = InStr(pos2 + 1, ad, "$")
pos4 = Len(ad) - (pos3)
Set premcelldep = Range(Left(ad, pos3 - 2))
Set derncelldep = Range(Right(ad, pos4))
liga2 = premcelldep.Row
cola2 = premcelldep.Column
ligz2 = derncelldep.Row
colz2 = derncelldep.Column


 Set plagedep2 = Range(Cells(liga2 - 1, 1), Cells(liga2 - 1, colz2))
    For Each cell In plagedep2
    If cell = "Sous-opération" Then
    colsousop = cell.Column
    End If
    If cell = "Montant Eligible" Then 'au départ le calcul était sur les dép raisonnée, mais c'était une erreur Montant présenté HT
    coldéprais = cell.Column
    End If
    If cell = "pourcentage des dépenses relevant du BBEA" Then
    coltxsoc = cell.Column
    End If
    If cell = "Postes" Then
    colpostes = cell.Column
    End If
Next

'collecte du taux socle
tauxsocle = Cells(liga2, coltxsoc)

' identification de la sous-opération principale et de la MAN, onglet DEPENSES dep
man = 0
sousopP = ""
volchsiqo = ""
décons = 0
cotbea = 0
cotbios = 0

i = 0
p = 0
'reprofilage du tableau qui va permettre d'enregistrer toutes les valeurs de dépenses en distinguant le cas des volailles de chair
ReDim tabl(ligz2, 12)
For Each cell2 In Range(Cells(liga2, colsousop), Cells(ligz2, colsousop))
    'cell2.Select 'à enlever
'collecte des dépenses mises aux normes à condition que le demandeur soit ja
    If cell2 = "Mise aux normes" And dja = "oui" Then
    man = man + cell2.Offset(0, coldéprais - colsousop)
    End If
' repérage des dépenses de déconstruction et application du plafonnement
    If cell2 = "Déconstruction" Then
    décons = décons + cell2.Offset(0, coldéprais - colsousop)
    If décons > 30000 Then décons = 30000
    End If

'collecte des dépenses éligibles hors mise aux normes
msgbox (cell2)
    If cell2 <> "Mise aux normes" And cell2 <> "Absence de sous-opération" And cell2 <> "Hors Opération" And cell2 <> "Hors assiette TO" And cell2 <> Empty And cell2 <> "Déconstruction" Then
        If cell2 <> sousopP And sousopP <> "" Then p = p + 1 'incrémentation des lignes et des colonnes du tableau
    ' pour déterminer la dominance BEA ou BIOS des dossiers
            If Right(cell2.Offset(0, colpostes - colsousop), 3) = "BEA" Then
            cotbea = cotbea + cell2.Offset(0, coldéprais - colsousop)
            End If
            If Right(cell2.Offset(0, colpostes - colsousop), 4) = "BIOS" And Right(cell2.Offset(0, colpostes - colsousop), 7) <> "BEABIOS" Then
            cotbios = cotbios + cell2.Offset(0, coldéprais - colsousop)
            End If
        
        sousopP = cell2
        ' pour les volailles de chair siqo, contrôler que pour une sous op de ce type , le plafond des 80 000€ n'est pas dépassé
        If Left(cell2, 33) = "Modernisation volaille chair SIQO" Then volchsiqo = "oui"
        tabl(i, p) = cell2.Value
        tabl(i, p + 6) = cell2.Offset(0, coldéprais - colsousop)
        i = i + 1
        End If
    
Next
' calcul du total dépenses raisonnées , onglet DEPENSES dep
déprais = 0 'montant des dépenses raisonnables hors MAN mais en fait c'est dépenses éligibles
dépraistemp = 0 'montant des dépenses raisonnables hors MAN par batiment volailles

If i > 0 Then
    For cpt2 = 6 To 12
        For cpt1 = 0 To i
        dépraistemp = tabl(cpt1, cpt2) + dépraistemp
'        cpt1 = cpt1 + 1
        Next
        If dépraistemp > 80000 And volchsiqo = "oui" Then ' Gestion des dépassements de plafond par bâtiment volailles siqo chair
        codvolchsiqo = "" '"le plafond de 80k€ est dépassé par bâtiment"
        déprais = 80000 + déprais
        Else
        déprais = dépraistemp + déprais
        End If
    dépraistemp = 0
'    cpt2 = cpt2 + 1
    Next
End If


'déprais = Application.WorksheetFunction.Sum(Range(Cells(liga2 , coldéprais), Cells(ligz2, coldéprais)))



' identification des TAUX ET PLAFONDS correspondant à la sous-opérationR , onglet GRILLES TAUX_PLAFONDS

Sheets("grilles taux_plafonds").Activate
Set plage3b = Range(Cells(liga3 + 1, cola3), Cells(ligz3, cola3))

' union de variable BOCE Bovin_lait, onglet grilles taux_plafonds

If dfil = "Bovin_lait" Or dfil = "Bovin_viande" Or dfil = "Caprin" Or dfil = "Ovin" Or dfil = "Equin" Then
 dfilH = "BOCE hors veaux de boucherie"
End If



'chargement du taux et du plafond du type de projet
If man > 0 Then codman = "oui"
If man = 0 Then codman = "non"
For Each cell In plage3b
If cell = dfil Or cell = dfilH Then
    cell.Offset(0, colsousopP - colfilierP).Activate
    'cas particulier des volaille siqo avec le déplafonnement, onglet grilles taux_plafonds
    If cell.Offset(0, colproj - colfilierP) = natproj And cell.Offset(0, colman - colfilierP) = codman Then                                    '22/06/21 cell.Offset(0, colsousopP - colfilierP) = sousopP And on pointe uniquement sur natpoj et dfil
    tauxdos = cell.Offset(0, coltauxP - colfilierP)
    plafdos = cell.Offset(0, colplafP - colfilierP)
    typroj = cell.Offset(0, coltyproj - colfilierP)
    typsoc = cell.Offset(0, coltypsoc - colfilierP)
    End If
End If
If 57 = cell.Row Then
cell.Select
End If
Next

'gestion de la feuille de SYNTHESE 4, onglet SYNTHESE

' en cas de Gaec, gestion des parts gaec
If dsociété = "GAEC" Then
    If dnbassos = 1 Then coefgaec = 1
    If dnbassos = 2 Then coefgaec = 1.8
    If dnbassos = 3 Then coefgaec = 2.1
    If dnbassos >= 4 Then coefgaec = 2.3
    Else
    coefgaec = 1
End If

' onglet SYNTHESE

Sheets("Synthèse").Activate
ligz4 = Cells(Rows.Count, 1).End(xlUp).Row
Set plage4 = Range(Cells(ligz4, 1), Cells(ligz4, 18))
'plage4.Select
ad = plage4.Address
pos1 = InStr(1, ad, "$")
pos2 = InStr(pos1 + 1, ad, "$")
pos3 = InStr(pos2 + 1, ad, "$")
pos4 = Len(ad) - (pos3)
Set premcellS = Range(Left(ad, pos3 - 2))
Set derncellG = Range(Right(ad, pos4))
liga4 = premcellG.Row
cola4 = premcellG.Column
ligz4 = derncellG.Row
colz4 = derncellG.Column

Set plage4a = Range(Cells(liga4, 1), Cells(liga4, colz4))
For Each cell In plage4a
    If cell = "taux d'aide" Then
    colStx = cell.Column
    End If
    If cell = "Numéro Osiris" Then
    colSnumosi = cell.Column
    End If
    If cell = "note" Then
    colSnote = cell.Column
    End If
    If cell = "dominance (BEA ou BIOS)" Then
    coldomi = cell.Column
    End If
    If cell = "dépenses éligibles plafonnées" Then
    colSplaf = cell.Column
    End If
    If cell = "montant de l'aide totale" Then
    colSmontaide = cell.Column
    End If
    If cell = "montant de l'aide modernisation" Then
    colSmontaidemod = cell.Column
    End If
    If cell = "montant de l'aide Man" Then
    colSmontaideMan = cell.Column
    End If
    If cell = "Projet BBEA" Then
    colSproj = cell.Column
    End If
    If cell = "taux socle" Then
    colStxsoc = cell.Column
    End If
    If cell = "types socle" Then
    colStypsoc = cell.Column
    End If
    If cell = "financeur Pacte" Then
    colSpacte = cell.Column
    End If
    If cell = "filière" Then
    colSfil = cell.Column
    End If
    If cell = "projet" Then
    cell.Select
    colSnatproj = cell.Column
    End If
    If cell = "Financeur Etat" Then
    cell.Select
    colSEtat = cell.Column
    End If
    If cell = "Financeur Région" Then
    cell.Select
    colSRégion = cell.Column
    End If
    If cell = "Financeur CD72" Then
    cell.Select
    colSCd72 = cell.Column
    End If
    If cell = "Feader" Then
    cell.Select
    colSFeader = cell.Column
    End If
    
 Next
 
 
'gestion de la Man et des plafonds, onglet  SYNTHESE
'pour mémoire : déprais est le total des dépenses raisonnées hors Man
'               plafdos est le plafond pour le projet pris dans la grille taux et plafonds
'               Man est le total des dépenses MAN

plafMan = 0  'plafond MAN
plafprojetmodman = 0  ' plafond mod et man cumulés
plafprojetmodmantrangaec = 0 '  plafond mod et man cumulés après modulation gaec
dépraismodplaf = 0 '  dépenses modernisation plafonnées

tauxMan = 0.4

If man > 0 Then       's'il y a de la MAN
plafMan = 30000         ' le plafond des aides est revu à la hausse
'plafprojetmodman = plafdos + plafMan   ' le nouveau plafond est calculé
'plafprojetmodmantrangaec = plafprojetmodman * coefgaec ' le calcul du plafond de dépenses totales
    If man > plafMan * coefgaec Then   ' si la MAN dépasse les 30 000€ par associé
'        codMan = "déplafonnementMan"            'il y a déplafonnement
            If man > 50000 * coefgaec Then      'il ne faut pas que la MAN dépasse les 50 000€
            man = 50000 * coefgaec              'dans ce cas elle est plafonnée
            End If          ' dans ce cas également les dépenses modernisation doivent être diminuées si jamais Man + Mod >plafondmodman
    'les dépenses modernisation sont plafonnées au plus petit montant des dépenses réélles ou plafond total moins man
        dépraismodplaf = Application.WorksheetFunction.Min(plafdos * coefgaec - man, déprais)
    'en cas de déconstruction
        If décons > 0 Then
        dépraismodplaf = Application.WorksheetFunction.Min(plafdos * coefgaec - man + décons, déprais)
        End If
    Else
        codman = "plafonnementManNormal" 'dans ce cas la man n'empiète pas sur la modernisation
        dépraismodplaf = Application.WorksheetFunction.Min(plafdos * coefgaec, déprais)
    'en cas de déconstruction
        If décons > 0 Then
        dépraismodplaf = Application.WorksheetFunction.Min(plafdos * coefgaec + décons, déprais)
        End If
    End If
Else: 's 'il n'y a pas de MAN

dépraismodplaf = Application.WorksheetFunction.Min(plafdos * coefgaec, déprais)
    'en cas de déconstruction
        If décons > 0 Then
        dépraismodplaf = Application.WorksheetFunction.Min(plafdos * coefgaec + décons, déprais)
        End If
End If

 
'calcul du taux déconstruction

tauxdécons = 0.3
'calcul si ja des taux

If dja = "oui" And dpja > 0 Then
tauxdos = tauxdos + 0.1 * dpja   'si ja la majoration est appliquée
    If man > 0 Then tauxMan = 0.4 + 0.1 * dpja     'si Man, le taux prend en compte la majoration ja
tauxdécons = 0.3 + 0.1 * dpja

End If


'calcul du montant des aides et affichage  SYNTHESE

     'cas où le nombre de bâtiments demandé en volailles siqo de chair est supérieur à celui autorisé en fonction des associés et du statut
If 2 * coefgaec < p + 1 And volchsiqo = "oui" Then
Cells(liga4 + 1, colSplaf) = "le nombre de bâtiments volailles de chair siqo dépasse le nombre autorisé"
Else
    If codvolchsiqo = "" Then  'dans le cas où il n'y a pas de pb particulier (volailles de chair), la déconstruction est comptée
    ' au taux de base
    montaide = (dépraismodplaf - décons) * tauxdos + décons * tauxdécons + man * tauxMan
    montaideMan = man * tauxMan
    montaideMod = (dépraismodplaf - décons) * tauxdos + décons * tauxdécons
    tauxglobal = montaide / (dépraismodplaf + man)   'plafprojetmodmantrangaec
    Cells(liga4 + 1, colStx) = tauxglobal
    Cells(liga4 + 1, colSmontaide) = montaide
    Cells(liga4 + 1, colSmontaideMan) = montaideMan
    Cells(liga4 + 1, colSmontaidemod) = montaideMod
    Cells(liga4 + 1, colSplaf) = dépraismodplaf + man  'plafprojetmodmantrangaec
    Cells(liga4 + 1, colSproj) = typroj
    
    If typsoc = "type3" Then Cells(liga4 + 1, colStxsoc) = tauxsocle
    Cells(liga4 + 1, colStypsoc) = typsoc
    Cells(liga4 + 1, colSfil) = dfil
    Cells(liga4 + 1, colSnatproj) = natproj
    Cells(liga4 + 1, colSnote) = note
    Else
     Cells(liga4 + 1, colSplaf) = codvolchsiqo
    End If


        'Cells(liga4 + 1, colStx).NumberFormat = "###.##%"
        'Cells(liga4 + 1, colStxsoc).NumberFormat = "###.##%"
        'Range(Cells(liga4 + 1, colSmontaide), Cells(liga4 + 1, colSmontaideMan)).NumberFormat = "### ###.##€"
        ' Range(Cells(liga4 + 1, colSEtat), Cells(liga4 + 1, colSFeader)).NumberFormat = "### ###.##€"
         
        'détermination du financeur pacte et autres financeurs
        
        financeur = ""
        If typroj = "BBEA" Then
        Select Case typsoc
            Case "type1"
            financeur = "Pacte"
                If département = 72 And dja = "oui" And man > 0 Then
                Cells(liga4 + 1, colSCd72) = montaideMan
                Cells(liga4 + 1, colSEtat) = montaide - montaideMan
                Else
                Cells(liga4 + 1, colSEtat) = montaide
                End If
            Case "type3"
             If tauxsocle >= 0.5 Then
             financeur = "Pacte"
                    If département = 72 And dja = "oui" And man > 0 Then
                    Cells(liga4 + 1, colSEtat) = montaide
                    Cells(liga4 + 1, colSCd72) = montaideMan
                    Cells(liga4 + 1, colSEtat) = montaide - montaideMan
                     Else
                    Cells(liga4 + 1, colSEtat) = montaide
                    End If
            Else: financeur = "non Pacte"
                 If département = 72 And dja = "oui" Then
                 Cells(liga4 + 1, colSRégion) = montaideMod * 0.47
                 Cells(liga4 + 1, colSCd72) = montaideMan * 0.47
                 Cells(liga4 + 1, colSFeader) = montaide - Cells(liga4 + 1, colSRégion) - Cells(liga4 + 1, colSCd72)
                 Else
                 Cells(liga4 + 1, colSRégion) = montaide * 0.47
                 Cells(liga4 + 1, colSFeader) = montaide - Cells(liga4 + 1, colSRégion)
                 End If
             End If
        End Select
        Else
                If département = 72 And dja = "oui" And man > 0 Then
                Cells(liga4 + 1, colSCd72) = montaideMan * 0.47
                Cells(liga4 + 1, colSFeader) = montaide * 0.53
                Cells(liga4 + 1, colSRégion) = montaide - montaide * 0.53 - montaideMan * 0.47
                Else
                    If man > 0 Then
                    Cells(liga4 + 1, colSFeader) = montaide * 0.53
                    Cells(liga4 + 1, colSEtat) = montaideMan * 0.47
                    Cells(liga4 + 1, colSRégion) = montaide - montaide * 0.53 - montaideMan * 0.47
                    Else
                    Cells(liga4 + 1, colSFeader) = montaide * 0.53
                    Cells(liga4 + 1, colSRégion) = montaide - montaide * 0.53
                    End If
                End If
        financeur = "non Pacte"
        End If
            
            Cells(liga4 + 1, colSpacte) = financeur

    ' détermination de la dominance BEA ou BIOS et inscription dans le tableau du résultat
    tauxdomi = 0
    If cotbea + cotbios > 0 Then
    tauxdomi = cotbea / (cotbea + cotbios)
        If tauxdomi >= 0.5 Then
        domi = "à Dominance bien-être animal"
        Else
        domi = "à Dominance Biosécurité"
        End If
    Else
    domi = "mixte BEA/BIOS"
    End If
    Cells(liga4 + 1, coldomi) = domi
    'numéro osiris
    Cells(liga4 + 1, colSnumosi) = dnumosi
End If


' vérification du tableau
If codvolchsiqo <> "" Then
cptsa = 0
cptsb = 0
For cptsa = 0 To i
    For cptsb = 0 To 12
    Cells(10 + cptsa, cptsb + 1) = tabl(cptsa, cptsb)
   Next cptsb
Next cptsa
End If

'Traitement des données pour préparer leur versement dans Osiris ONGLET DEPENSES OSIRIS

nomfich = "dépenses"
Sheets(nomfich).Activate
Application.CutCopyMode = False
plagedep.Copy
nomfich = "dépenses osiris"
Sheets(nomfich).Activate
Cells(3, 1).Select
ActiveSheet.Paste


For Each cell In Range(Cells(liga2, colpostes), Cells(ligz2, colpostes))

'cas des postes composés avec _BEA ou _BIOS ou _BEABIOS
If cell <> "BEA" And cell <> "BIOS" And cell <> "BEABIOS" Then
    longposte = Len(cell)
    
    'cas des postes composés IMM_BEA ou IMM_BIOS
    If cell <> "IMM_BEA" And cell <> "IMM_BIOS" And cell <> "IMM_BEABIOS" Then
        If Right(cell, 3) = "BEA" Then
        cell.Value = Left(cell, longposte - 3)
        End If
        If Right(cell, 4) = "BIOS" Then
            If Right(cell, 7) = "BEABIOS" Then
            cell.Value = Left(cell, longposte - 7)
            Else
            cell.Value = Left(cell, longposte - 4)
            End If
        End If
    Else
        If cell = "IMM_BEA" Or cell = "IMM_BIOS" Then
        cell.Value = "IMM_BEABIOS"
        End If
    End If
End If

'cas des postes simples BEA ou BIOS
If cell = "BEA" Or cell = "BIOS" Then
cell.Value = "BEABIOS"
End If

'cas des postes composés BEA_AUTO ou BIOS_AUTO
If cell = "BEA_AUTO" Or cell = "BIOS_AUTO" Then
cell.Value = "BEABIOS_AUTO"
End If


Next

Columns(coldéprais + 1).Delete
Columns(coldéprais + 1).Delete
Columns(coldéprais + 1).Delete
Columns(coldéprais + 1).Delete
Columns(coldéprais).Delete
Cells(1, 1).Activate

Call export_csv
Sheets("Synthèse").Activate
Cells(1, 1).Activate

End Sub

Sub remettre_à_zéro()
	Sheets("synthèse").Activate
	Range(Cells(4, 1), Cells(30, 19)) = ""
	Sheets("dépenses osiris").Activate
	Range(Cells(3, 1), Cells(100, 11)) = ""
	Sheets("synthèse").Activate
	Cells(1, 1).Select
End Sub

Sub remettre_à_zéro_onglet_dépenses()
	efftab = MsgBox("Etes vous sûr de vouloir effacer toutes les données du tableau de dépenses ?", vbYesNo)
	Select Case efftab
		Case 6
		Sheets("dépenses").Activate
		Range(Cells(3, 2), Cells(55, 13)) = ""
		Cells(1, 1).Select
	End Select
End Sub


Sub export_csv()

 Dim objShell As Object, objFolder As Object, oFolderItem As Object
 Sheets("dépenses osiris").Activate
csvrep = MsgBox("voulez vous enregistrer la sortie Osiris dans un fichier csv ?", vbYesNo)
    Select Case csvrep
    Case 6
ligz = Cells(2000, 5).End(xlUp).Row
Set plage5 = Range(Cells(ligz, 5), Cells(ligz, 5)).CurrentRegion
   ' For Each cell In Selection
   ' If cell.NumberFormat = "$#,##0.00" Then
   ' cell.NumberFormat = "###0.00"
   ' End If
   ' Next
ad = plage5.Address
pos1 = InStr(1, ad, "$")
pos2 = InStr(pos1 + 1, ad, "$")
pos3 = InStr(pos2 + 1, ad, "$")
pos4 = Len(ad) - (pos3)
Set premcellO = Range(Left(ad, pos3 - 2))
Set derncellO = Range(Right(ad, pos4))
liga5 = premcellO.Row
cola5 = premcellO.Column
ligz5 = derncellO.Row
colz5 = derncellO.Column
Set plage5b = Range(Cells(liga5, cola5 + 1), Cells(ligz5 + 1, colz5))
    plage5b.Select
    plage5b.Copy
    Workbooks.Add
    ActiveSheet.Paste
    Application.CutCopyMode = False
    Application.DisplayAlerts = False
    Columns(1).Insert
    Cells(1, 1).Value = "N°"
        Set objShell = CreateObject("Shell.Application")
        Set objFolder = objShell.BrowseForFolder(&H0&, "Choisir un répertoire", &H1&)
        nomcsv = InputBox("indiquer le nom du fichier csv à sauvegarder :")
     
        On Error Resume Next
        Set oFolderItem = objFolder.Items.Item
        Chemin = oFolderItem.Path
    
        ActiveWorkbook.SaveAs Filename:=Chemin & "\" & nomcsv, FileFormat:=xlCSV, CreateBackup:=False, Local:=True
        ActiveWindow.Close
        Application.DisplayAlerts = True
        
    Case 7
    
End Select
End Sub

Sub ChoixRepertoire()
   Dim objShell As Object, objFolder As Object, oFolderItem As Object
    Dim Chemin As String
 
    Set objShell = CreateObject("Shell.Application")
    Set objFolder = objShell.BrowseForFolder(&H0&, "Choisir un répertoire", &H1&)
 
    On Error Resume Next
    Set oFolderItem = objFolder.Items.Item
    Chemin = oFolderItem.Path
 
    MsgBox Chemin
End Sub

Sub essaival()
Dim cell As Range

    For Each cell In Selection
    If cell.NumberFormat = "#,##0.00 $" Then
    cell.NumberFormat = "###0.00"
    End If
    Next
End Sub

