Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
Sub PCAE_feuille_calc_2021_taux50()
'la macro permet le calcul des 50% investissements BBEA

'référencement du tableau de dépenses
nomfich = "dépenses"
Sheets(nomfich).Activate

ligz = Cells(2000, 5).End(xlUp).Row

Set plage = Range(Cells(3, 1), Cells(ligz, 12))

'plage.Select
ad = plage.Address
pos1 = InStr(1, ad, "$")
pos2 = InStr(pos1 + 1, ad, "$")
pos3 = InStr(pos2 + 1, ad, "$")
pos4 = Len(ad) - (pos3)
Set premcell = Range(Left(ad, pos3 - 2))
Set derncell = Range(Right(ad, pos4))
premcell.Select
'derncell.Select

liga = premcell.Row
cola = premcell.Column
ligz = derncell.Row
colz = derncell.Column


'référencement  : identification des étiquettes"
            
                Set plage = Range(Cells(liga - 1, 1), Cells(liga - 1, colz + 1))
                For Each cell In plage
                    If cell = "Postes" Then
                    colpostes = cell.Column
                    End If
                    If cell = "Sous-opération" Then
                    colsousop = cell.Column
                    End If
                    If cell = "Montant Eligible" Then
                    colélig = cell.Column
                    End If
                    If cell = "Montant Raisonné" Then
                    colRais = cell.Column
                    End If
                    If cell = "pourcentage des dépenses relevant du BBEA" Then
                    colprcbbea = cell.Column
                    End If
                Next cell
             
'fin de réferencement

'Calcul des 50% d'investissements BBEA et de la mise aux normes
comptbbea = 0
comptman = 0
compttot = 0
lig = 0
For Each cell In Range(Cells(liga, colpostes), Cells(ligz, colpostes))
    lig = cell.Row
    cod = Right(cell, 3)
    'sont exclus les sous-op hors opération et hors assiette to
    eti = cell.Offset(0, colsousop - colpostes).Value
    If eti <> "Hors Opération" And eti <> "Hors assiette TO" And eti <> "Absence de sous-opération" Then
        ' les valeurs pour le total sont décomptés
        compttot = Cells(lig, colélig) + compttot
        ' les valeurs BBEA sont décomptées
        If Right(cell, 3) = "BEA" Or Right(cell, 3) = "IOS" Then
        comptbbea = Cells(lig, colélig) + comptbbea
        End If
        If Right(cell, 3) = "MAN" Or cell = "BAT_GEF_MAN_AUTO" Or Left(cell, 9) = "IMM_DEXEL" Then
        comptman = Cells(lig, colélig) + comptman
        End If
    End If

Next cell

'le pourcentage est :
If (compttot - comptman) <> 0 Then
pourcbbea = comptbbea / (compttot - comptman)
Cells(liga, colprcbbea) = pourcbbea
'Cells(liga, colprcbbea).NumberFormat = "###.##%"
End If

Cells(liga, colpostes).Activate
End Sub

